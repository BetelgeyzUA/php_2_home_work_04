<VirtualHost *:80>
    ServerAdmin {{ServerAdmin}}
    ServerName {{ServerName}}
    DocumentRoot {{DocumentRoot}}
    <Directory {{DocumentRoot}}>
        AllowOverride All
        Allow from All
    </Directory>
    ErrorLog /var/log/apache2/{{ServerName}}/error.log
    CustomLog /var/log/apache2/{{ServerName}}/access.log combineds
</VirtualHost>