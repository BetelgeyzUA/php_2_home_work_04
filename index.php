<?php


class Setting {

    /*
     * return boolean
     */

    protected function check_file_exist($file) {
        return file_exists($file);
    }

    /*
     * return boolean
     */

    protected function check_dir_access($dir) {
        if(!is_writable($dir)) {
            echo 'Using this program with root permission'.PHP_EOL;
            die();
        }

        return true;
    }

    protected function press_key(){
        return intval(ord((fgets(STDIN))));
    }

    /*
     * return string
     */

    protected function line () {
        echo '---------------------------------------------------------------'.PHP_EOL;
    }
}

class App extends  Setting {

    public $key;

    public function __construct()

    {
        $this->init();
    }

    public function init () {

        $this->check_dir_access('/etc/apache2/');

        $this->line();

        echo 'Welcom to program create virtual host'.PHP_EOL;

        $this->line();

        echo 'If you want to create a host input key \'1\' else del key \'0\','.PHP_EOL;
        echo 'Enter key ';

        $key = $this->press_key();

        if($key == 49) {
            $this->key = $key;
        } elseif ($key == 48) {
            $this->key = $key;
        } else {
            echo 'Invalid input key'.PHP_EOL;
            die();
        }
    }


}

class Host extends  Setting{

    protected $template;

    protected $config;

    public function __construct()

    {
        $this->init();
    }

    protected function init () {
        $this->create_template();
        $this->get_json();
        $this->template = strtr($this->template, $this->config);
        $this->set_config_site($this->config['{{ServerName}}'], $this->template);
        $this->create_log_dir($this->config['{{ServerName}}']);
        $this->create_dir_host($this->config['{{DocumentRoot}}']);
        $this->create_string_host($this->config['{{ServerName}}']);
        $this->apache_restart();
    }

    protected function create_template() {
        if ($this->check_file_exist('template.php')) {
            $this->template = file_get_contents('template.php');
        } else {
            echo 'Can\'t find file \'template.php\''.PHP_EOL;
        }
    }

    protected  function get_json() {
        if($this->check_file_exist('vs.json')){
            $str = file_get_contents('vs.json');
            $config = json_decode($str, true);
            $required = ['ServerName', 'ServerAdmin', 'DocumentRoot'];
                foreach ($required as $value) {
                    if(empty($config[$value])) {
                        echo 'param '.$value.' required.'.PHP_EOL;
                        die();
                    }
                }
            $result = [];
                foreach ($config as $key => $value) {
                    $result['{{'.$key.'}}'] = $value;
                }
            $this->config = $result;
        } else {
            echo 'Can\'t find file \'vs.json\'';
            echo '{"ServerName": "", "ServerAdmin": "",  "DocumentRoot": ""}'."\n";
            die();
        }
    }

    protected function set_config_site ($file, $template) {

        $file_conf = '/etc/apache2/sites-available/'.$file.'.conf';

        if($this->check_file_exist($file_conf)) {
            $this->line();
            echo 'file '.$this->config['{{ServerName}}'].' exist'.PHP_EOL ;
            echo 'If you want to continue enter key \'1\', you will overwrite the file'.PHP_EOL;
            echo 'Enter the \'0\' key to terminate the program.'.PHP_EOL;
            $this->line();
            echo 'Enter key ';

            $key = $this->press_key();
            if($key == 49) {
                file_put_contents($file_conf, $template);
                echo 'file '.$file.' is overwritten'.PHP_EOL;
                $this->line();
            } elseif ($key == 48) {
                echo 'Close program'.PHP_EOL;
                die();
            } else {
                echo 'Invalid input key'.PHP_EOL;
                die();
            }


        } else {
            file_put_contents($file_conf, $template);
            echo 'file '.$file.' create'.PHP_EOL;
            $this->line();
        }

        exec('a2ensite '.$file.'.conf', $output);

        if (isset($output[0])) {
            echo $output[0].PHP_EOL;
        }

    }

    protected function create_dir($dir_path, $dir) {
        if ($this->check_file_exist($dir_path)) {
            echo 'dir '.$dir.' exist'.PHP_EOL;
            $this->line();
        } else {
            exec('mkdir  '.$dir_path, $output);
            if($this->check_file_exist($dir_path)) {
                echo 'File '.$dir_path.' create'.PHP_EOL;
                $this->line();
            }
        }
    }

    protected function create_log_dir($dir) {

        $dir_path = '/var/log/apache2/'.$dir;

        $this->create_dir($dir_path, $dir);

    }

    protected function create_dir_host ($dir) {

            $dir_path = $dir;

            $this->create_dir($dir_path, $dir);
    }

    protected function create_string_host ($name) {

        $file_path = '/etc/hosts';

        if($this->check_file_exist($file_path)) {
            $text = PHP_EOL.'# Config file create'.PHP_EOL;
            $text .= '127.0.0.1 '.$name;

            file_put_contents($file_path, $text, FILE_APPEND);

        } else {
            echo 'dir /etc/hosts not exist'.PHP_EOL;
            $this->line();
        }
    }

    protected function apache_restart() {
        exec('apache2ctl restart', $output);

        if (isset($output[0])) {
            echo $output[0].PHP_EOL;
        }
    }


}

class Del extends  Setting{

}

$app = new App();

if ($app->key == 49) {
    $host = new Host();
} else if ($app->key == 48) {
    $host = new Del();
}
